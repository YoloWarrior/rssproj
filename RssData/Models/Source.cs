﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RssData.Models
{
 public   class Source
    {
        public Guid Id { get; set; }
        public string RSSSourceURL { get; set; }
        public string SourceName { get; set; }
        public virtual List<News> News { get; set; }
    }
}
