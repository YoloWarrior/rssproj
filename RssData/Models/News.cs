﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RssData.Models
{
  public  class News
    {
        public Guid Id { get; set; }
        public string NewsName { get; set; }
        public DateTime Date { get; set; }
        public string Description { get; set; }
        public string NewsURL { get; set; }
        public Guid SourceID { get; set; }
        public Source Source { get; set; }
    }
}
