﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace RssData.Repositories
{
    public class GenereticRepos<T> : IRepos<T> where T : class, new()
    {
        private DataContext _context;
        private DbSet<T> db;

        public GenereticRepos(DataContext context)
        {
            _context = context;
            db = _context.Set<T>();
        }
        public void Create(List<T> item)
        {
            db.AddRange(item);
            _context.SaveChanges();
        }
        public void Create(T item)
        {
            db.Add(item);
            _context.SaveChanges();
        }

        public T FindWithInclude(Func<T, bool> predicate, params Expression<Func<T, object>>[] includeProperties)
        {
            return Include(includeProperties).Where(predicate).SingleOrDefault();
        }

        public T Get(Func<T, bool> predicate)
        {
            return db.AsNoTracking().SingleOrDefault(predicate);
        }

        public IEnumerable<T> GetAll()
        {
            return db;
        }


        public IEnumerable<T> GetAllWithInclude(params Expression<Func<T, object>>[] includeProperties)
        {
            return Include(includeProperties).ToList();
        }
       
        private IQueryable<T> Include(params Expression<Func<T, object>>[] includeProperties)
        {
            var query = db.AsQueryable();
            foreach (var include in includeProperties)
                query = query.Include(include);
            return query;
        }
    }
}
