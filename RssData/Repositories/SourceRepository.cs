﻿using RssData.Interfaces;
using RssData.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace RssData.Repositories
{
    class SourceRepository : GenereticRepos<Source>, ISource
    {
        public SourceRepository(DataContext context) : base(context) { }
    }
}
