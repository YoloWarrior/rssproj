﻿using RssData.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace RssData.Repositories
{
    public class ReposWrapper : IReposWrapper
    {
        private DataContext _db;
        private readonly INews _news;
        private readonly ISource _source;


        public ReposWrapper(DataContext context)
        {
            _db = context;
        }
        public INews News => _news ?? new NewsRepository(_db);

        public ISource Source => _source ?? new SourceRepository(_db);

    }
}
