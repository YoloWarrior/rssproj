﻿using RssData.Interfaces;
using RssData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RssData.Repositories
{
    class NewsRepository:GenereticRepos<News>,INews
    {
        DataContext _context;
        public NewsRepository(DataContext context) : base(context) {
            _context = context;
        }

        public bool Check(DateTime time, string text)
        {
            bool cond;
         cond =  _context.News.Where(s => s.Date == time.Date&&s.NewsName==text).FirstOrDefault() != null ?
                 false : true;

            return cond;
        }
    }
}
