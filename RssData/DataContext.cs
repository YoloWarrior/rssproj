﻿
using Microsoft.EntityFrameworkCore;
using RssData.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace RssData
{
    public class DataContext : DbContext
    {
        public DbSet<News> News { get; set; }
        public DbSet<Source> Sources { get; set; }

        public DataContext()
        {
            Database.EnsureCreated();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=RssDB;Trusted_Connection=True");
        }
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<News>()
           .HasOne(p => p.Source)
           .WithMany(x => x.News)
           .HasForeignKey(x => x.SourceID)
            .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
