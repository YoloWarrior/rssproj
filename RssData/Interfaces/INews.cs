﻿using RssData.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace RssData.Interfaces
{
 public   interface INews:IRepos<News>
    {
        bool Check(DateTime time, string text);
    }
}
