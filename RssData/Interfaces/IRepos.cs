﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace RssData
{
  public  interface IRepos<T> where T : class
    {
        IEnumerable<T> GetAll();
        IEnumerable<T> GetAllWithInclude( params Expression<Func<T, object>>[] includeProperties);
        T FindWithInclude(Func<T, bool> predicate, params Expression<Func<T, object>>[] includeProperties);

        T Get(Func<T, bool> predicate);
        void Create(T item);
        void Create(List<T> item);
    }
}
