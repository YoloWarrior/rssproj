﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RssData.Interfaces
{
  public  interface IReposWrapper
    {
        INews News { get; }
        ISource Source { get; }
    }
}
