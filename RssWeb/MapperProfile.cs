﻿using AutoMapper;
using RssData.Models;
using RssWeb.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RssWeb
{
    public class MapperProfile:Profile
    {
        public MapperProfile()
        {
            CreateMap<News, NewsViewModel>();
            CreateMap<Source, SourceViewModel>();
        }
    }
}
