﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RssWeb.ViewModel
{
    public class NewsViewModel
    {
        public string NewsName { get; set; }
        public DateTime Date { get; set; }
        public string Description { get; set; }
        public string NewsURL { get; set; }
        public Guid SourceID { get; set; }
    }
}
