﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RssWeb.ViewModel
{
    public class SourceViewModel
    {
        public string RSSSourceURL { get; set; }
        public string SourceName { get; set; }
        public virtual List<NewsViewModel> News { get; set; }
    }
}
