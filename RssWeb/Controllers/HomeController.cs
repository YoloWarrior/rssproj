﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using RssData.Interfaces;
using RssData.Models;
using RssWeb.ViewModel;

namespace RssWeb.Controllers
{
    [Route("api/[controller]")]
    public class HomeController : Controller
    {
        IReposWrapper _repos;
        IMapper _mapper;
        private int size=5;
        private int start = 0;
        public HomeController(IReposWrapper repos,IMapper mapper)
        {
            _repos = repos;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetPage")]
        public object GetPage()
        {
             var getPage = (start, size);
            return getPage;
        }
        [HttpGet]
        public IEnumerable<SourceViewModel>Get(int startPage, string sort = "",string filt="")
            {
            start = startPage == 0 ? start : startPage;
            var news = _repos.Source.GetAllWithInclude(x => x.News);
            if (filt != null && filt!="All")
            {
                news = news.Where(x => x.SourceName == filt);
                size = 10;
            }
           
            start = start <= 0 ? start = 0 : start;

            foreach (var p in news)
            {
                switch (sort)
                {
                    case null:
                       p.News = p.News.Skip(start).Take(size).ToList();

                        break;
                    case "Date":
                        p.News = p.News.OrderBy(x => x.Date).Skip(start).Take(size).ToList();
                        break;
                    case "Source":
                        p.News = p.News.OrderBy(x => x.Source.SourceName).Skip(start).Take(size).ToList();
                        break;
                } 
            }
            return _mapper.Map<IEnumerable<SourceViewModel>>(news);
        }
    }
}
