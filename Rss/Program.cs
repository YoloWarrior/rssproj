﻿using Microsoft.Extensions.DependencyInjection;
using RssData;
using RssData.Interfaces;
using RssData.Models;
using RssData.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Text.RegularExpressions;
using System.Xml;

namespace Rss
{

    class Program
    {

        private static IReposWrapper DI()
        {
            var serviceProvider = new ServiceCollection()
                  .AddScoped<IReposWrapper, ReposWrapper>()
                  .AddDbContext<DataContext>()
                  .BuildServiceProvider();

            IReposWrapper repo;
            return  serviceProvider.GetService<IReposWrapper>();

        }

       static int UpdateDB(string url)
        {
            int count = 0;

            var _repos = DI();

            XmlReader reader = XmlReader.Create(url);
                try
                {
                    SyndicationFeed rssdata = SyndicationFeed.Load(reader);
                    foreach (var item in rssdata.Items)
                    {

                        if (_repos.News.Check(item.PublishDate.DateTime, item.Title.Text.ToString()))
                        {
                            count++;
                            _repos.News.Create((new News
                            {
                                Date = item.PublishDate.DateTime,
                                Description = Regex.Replace(item.Summary.Text.ToString(),
                                @"<[^>]*>", String.Empty),
                                NewsName = item.Title.Text.ToString(),
                                NewsURL = item.Id.ToString(),
                                SourceID = _repos.Source.FindWithInclude(x=>x.RSSSourceURL==url,x=>x.News).Id
                            }));

                        }

                    }

                }
                catch
                {

                    Console.WriteLine("Источни: " + url + " недоступен");


                }
                finally
                {

                    reader.Close();
                }
                Console.WriteLine("Всего новстей в истонике: " + _repos.Source.GetAllWithInclude(x=>x.News).Count());
            

            return count;
        }


        static void Main(string[] args)
        {
       

            var _repos = DI();
            List<Source> source = new List<Source>();

            _repos.Source.Create(new List<Source> {
                new Source {Id=Guid.NewGuid(),RSSSourceURL="https://interfax.by/news/feed/",SourceName="Интерфакс" },
                new Source {Id = Guid.NewGuid(), RSSSourceURL = "https://habr.com/ru/rss/all/all/", SourceName = "Хабр" }
            });
            

       IEnumerable<News> news = _repos.News.GetAll();
            IEnumerable<Source> sources = _repos.Source.GetAll();

            foreach (var item in sources)
            {

                int updated = UpdateDB(item.RSSSourceURL);
                Console.WriteLine("Источник :" + item.SourceName);
                Console.WriteLine("Добавлено новостей: {0}", updated);


                Console.WriteLine(new string('-', 50));
            }


            Console.ReadLine();
        }
    }
}
